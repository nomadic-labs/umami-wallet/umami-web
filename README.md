# Umami - Website

This repo contains the website code for the website formerly intended for the following domains:
 * https://umamiwallet.com 
 * https://www.umamiwallet.com 
 * https://umami.cash
 * https://www.umami.cash
 * https://umamini.com 
 * https://www.umamini.com 


 <!--  For root domain, configure the DNS with : https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#for-root-domains -->

## Authors
Thanks to the contributors to this project (2020-2023) :
- Samuel Bourque @SamREye
- Julia Ah-Yionne @Juahyio
- Rémy El Sibaïe @remyzorg
- Julien Sagot @sagotch 
- Corentin Méhat @comeh 

## Deprecation
This project has been deprecated.
Further developments will be done here : https://github.com/trilitech/umami-v1 
